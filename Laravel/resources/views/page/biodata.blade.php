<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width-device-width, initial-scale-1.0">
        <meta http-equiv="X-UA-Compatible" content="ie-edge">
        <title>Document</title>

    </head>
    <body>
        <h1>Buat Account Baru!</h1>
            <h3>Sign Up Form</h3>
            
            <form action="/kirim" method="POST">
                @csrf
                <label>First name:</label><br>
                <input type = "text" name="fname"><br><br>
                <label>Last name:</label><br>
                <input type = "text" name="lname"><br><br>
                <label>Gender:</label><br>
                <input type = "radio">Male<br>
                <input type = "radio">Female<br>
                <input type = "radio">Other<br><br>
                <label>Nationality:</label><br>
                <select name="nationality">
                    <option value="1">Indonesian</option>
                    <option value="2">Malaysian</option>
                    <option value="3">Singaporean</option>
                    <option value="4">Australian</option>
                </select><br><br>
                <label>Languange Spoken:</label><br>
                <input type = "checkbox" name = "1">Bahasa Indonesia<br>
                <input type = "checkbox" name = "2">English<br>
                <input type = "checkbox" name = "3">Other<br><br>
                <label>Bio:</label><br>
                <textarea name = "bio" row="10" cols="20"></textarea><br><br>
                <input type="submit" value="Sign Up">
            </form>
    </body>
</html>