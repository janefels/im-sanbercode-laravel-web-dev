@extends('layout.master')
@section('title')
Detail Cast
@endsection
@section('content')
    <h1>{{$cast->nama}}</h1>
    <p>Umur : {{$cast->umur}} tahun</p>
    <p>{{$cast->bio}}</p>
@endsection