@extends('layout.master')
@section('title')
Add Cast
@endsection
@section('content')
    <form action="/cast" method="POST">
        @csrf
        <div class="form-group">
            <label>Nama:</label><br>
            <input type = "text" name="nama" class="form-control">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <br>
        <div class="form-group">
            <label>Umur:</label><br>
            <input type = "text" name="umur" class="form-control">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <br>
        <div class="form-group">
            <label>Bio:</label><br>
            <textarea name = "bio" class="form-control" row="10" cols="20"></textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <br>
        <input type="submit" class="btn btn-primary" value="Add">
    </form>
@endsection