@extends('layout.master')
@section('title')
Show Cast
@endsection
@section('content')
    <a href="/cast/create" class="btn btn-primary btn-sm my-2">Tambah</a>
    <table class="table">
        <thead>
          <tr>
            <th scope="col">No</th>
            <th scope="col">Name</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key => $item)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$item->nama}}</td>
                    <td>
                        <form action="/cast/{{$item->cast_id}}" method="post">
                            @csrf
                            @method('delete')
                            <a href="/cast/{{$item->cast_id}}" class="btn btn-info btn-sm">Detail</a>
                            <a href="/cast/{{$item->cast_id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                            <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td>Data Cast Kosong</td>
                </tr>
            @endforelse
        </tbody>
      </table>
@endsection