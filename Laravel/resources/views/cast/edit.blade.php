@extends('layout.master')
@section('title')
Edit Cast
@endsection
@section('content')
    <form action="/cast/{{$cast=>$cast_id}}" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Nama:</label><br>
            <input type = "text" value={{$cast->nama}} name="nama" class="form-control">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <br>
        <div class="form-group">
            <label>Umur:</label><br>
            <input type = "text" value= {{$cast->umur}} name="umur" class="form-control">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <br>
        <div class="form-group">
            <label>Bio:</label><br>
            <textarea name = "bio" class="form-control" row="10" cols="20">{{$cast->bio}}</textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <br>
        <input type="submit" class="btn btn-primary" value="Add">
    </form>
@endsection