<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create(){
        return view('cast.add');
    }

    public function store(request $request){
        //validasi data
        $request->validate([
            'nama' => 'required|max:225|min:5',
            'umur' => 'required|integer',
            'bio' => 'required|min:5'
        ]);

        //masukkan data request ke table cast di database
        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);

        //lempar ke halaman /cast
        return redirect('/cast');
    }
    public function index(){
        $cast = DB::table('cast')->get();

        return view('cast.show', ['cast' =>$cast]);
    }

    public function show($cast_id){
        $cast = DB::table('cast')->find($cast_id);

        return view('cast.detail', ['cast' => $cast]);
    }

    public function edit($cast_id){
        $cast = DB::table('cast')->find($cast_id);

        return view('cast.edit', ['cast' => $cast]);
    }

    public function update($cast_id, Request $request){
        //validasi data
        $request->validate([
            'nama' => 'required|max:225|min:5',
            'umur' => 'required|integer',
            'bio' => 'required|min:5'
        ]);

        DB::table('cast')
                ->where('cast_id',$cast_id)
                ->update(
                    [
                        'nama' => $request['nama'],
                        'umur' =>$request['umur'],
                        'bio' =>$request['bio'],
                    ]
                    );
                //lempat ke url / cast
                return redirect('/cast');
    }

    public function delete($cast_id){
        DB::table('cast')->where('cast_id', $cast_id)->delete();

        return redirect('/cast');
    }
}
