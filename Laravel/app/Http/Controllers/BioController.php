<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BioController extends Controller
{
    public function daftar(){
        return view('page.biodata');
    }

    public function kirim(Request $request){
        $namaDepan = $request['fname'];
        $namaBelakang = $request['lname'];
        return view('page.welcome', ["namaDepan"=> $namaDepan, "namaBelakang"=> $namaBelakang]);
    }
}
