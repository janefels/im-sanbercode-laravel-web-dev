<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\BioController;
use App\Http\Controllers\CastController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [IndexController::class, 'dashboard']);
Route::get('/daftar', [BioController::class, 'daftar']);
Route::post('/kirim', [BioController::class, 'kirim']);
Route::get('/data-table',function(){
    return view('page.datatable');
});
Route::get('/table',function(){
    return view('page.table');
});

//CRUD
//Create Data
//route untuk mengarah ke form 
Route::get('/cast/create', [CastController::class, 'create']);
//route untuk menyimpan data inputan ke DB table cast
Route::post('/cast', [CastController::class, 'store']);

//Read Data
//Route untuk menampilkan semua data yang ada di table cast database
Route::get('/cast', [CastController::class, 'index']);
//Route untuk ambil detail berdasarkan id
Route::get('/cast/{cast_id}', [CastController::class, 'show']);

//Update Data
//route untuk mengarah ke form edit cast dengan membawa data berdasarkan id
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
//Route untuk update cast berdasarkan id
Route::put('/cast/{cast_id}', [CastController::class, 'update']);

//Delete Data
//Route untuk hapus data berdasarkan id cast
Route::delete('/cast/{cast_id}', [CastController::class, 'delete']);