<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');


$animal = new Animal("shaun");
echo "Name : ". $animal->name ."<br>";
echo "Legs : ". $animal->legs ."<br>";
echo "Cold Blooded : ". $animal->cold_blooded ."<br><br>";

$animalFrog = new frog("buduk");
echo "Name : ". $animalFrog->name ."<br>";
echo "Legs : ". $animalFrog->legs . "<br>";
echo "Cold Blooded : ". $animalFrog->cold_blooded ."<br>";
echo $animalFrog->jump() ."<br><br>";

$animalApe = new ape("kera sakti");
echo "Name : ". $animalApe->name ."<br>";
echo "Legs : ". $animalApe->legs ."<br>";
echo "Cold Blooded : ". $animalApe->cold_blooded ."<br>";
echo $animalApe->yell() ."<br>";
?>